struct auto{
  char modelo;
  char nombre[30];
  char motor[30];
  int anio;
};

struct computadora{
  char procesador[20];
  int memoriaRam;
  char tarjetaGrafica[30];
  char motherboard[30];
};

struct alumno{
  char nombre[30];
  char carrera[30];
  int edad;
  int numCuenta;
};

struct equipoFut{
  char nombre[30];
  char pais[30];
  int jugadores;
  int victorias;
};

struct mascota{
  char nombre[20];
  char animal[20];
  int edad;
  char sexo[20];
};

struct videojuego{
  char titulo[30];
  char dificultad[20];
  char plataforma[20];
  int jugadores;
};

struct superHeroe{
  char alias[30];
  char identidadSecreta[30];
  char poder[30];
  int enemigos;
};

struct planeta{
  int masa;
  int diametro;
  int distanciaAlSol;
  char nombre[30];
};

struct personajeJuego{
  char nombreJUego[30];
  char nombrePersonaje[30];
  char genero[20];
  float estatura;
};

struct comic{
  char titulo[30];
  char numeroComic[30];
  int paginas;
  float precio;
};
